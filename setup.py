import sys, os, json
from os.path import join

from distutils.core import setup
from distutils.command.install import install

from jupyter_client.kernelspec import KernelSpecManager
from IPython.utils.tempdir import TemporaryDirectory


# TODO: Share this with the install code.
kernel_json = {
    'argv': [sys.executable, '-m', 'jupyter_postgres_kernel', '-f', '{connection_file}'],
    'display_name': 'PostgreSQL',
    'language': 'sql',
    'codemirror_mode': 'sql',
}


class _install(install):
    # Subclasses the install command to register the kernel after installing the code.  (The
    # Jupyter project should really consider renaming "installing a kernel spec" to
    # "registering a kernel".)
    def run(self):
        install.run(self)

        with TemporaryDirectory() as td:
            os.chmod(td, 0o755)
            with open(join(td, 'kernel.json'), 'w') as f:
                json.dump(kernel_json, f, sort_keys=True)

            print('Installing kernel spec')
            KernelSpecManager().install_kernel_spec(td, 'pglib', replace=True)


setup(
    name='jupyter_postgres_kernel',
    version='0.1.0',  # TODO: Fix this
    description='A PostgreSQL kernel for Jupyter notebooks',
    author='Michael Kleehammer',
    author_email='michael@kleehammer.com',
    url='https://gitlab.com/mkleehammer/jupyter_postgres_kernel',
    packages=['jupyter_postgres_kernel'],
    cmdclass={'install': _install},
    install_requires=['pglib', 'tabulate'],
    classifiers=[
        'Framework :: Jupyter',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Topic :: System :: Shells'
    ]
)
