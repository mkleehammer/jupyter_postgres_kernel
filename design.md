

# Questions

## Connection Parameters

Using a cell with a hardcoded format is probably easiest.  One option is to use psql's \c
command.  It allows connections to other hosts and ports.  We would need to add password
support at some point.

## Variables

Variables can greatly simplify things.  I suggest we use the format `$name` which is similar to
PostgreSQL's `$0` format but allows us to use a global parameter namespace.

- Do we allow variables?  If so, how do we set them and use them?
  - We could set them using a cell that matches `<var>=<value>` where value is a string, date,
    or number.
  - Using them could be as simple as `$varname`.
  - We could set them from the previous select statement by column name: `$x = row.col` or
    `$x = row[0]`.

Is the dollar sign required when setting.  I think so.

How are they viewed?  Since it is going to require 
