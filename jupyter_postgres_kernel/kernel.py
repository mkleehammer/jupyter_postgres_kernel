from ipykernel.kernelbase import Kernel
import re, logging
import pglib
from tabulate import tabulate

__version__ = '0.1.0'

logger = logging.getLogger('postgres_kernel')


class DBKernel(Kernel):
    implementation = 'pg_kernel'
    implementation_version = __version__

    banner = 'PostgreSQL (pglib)'

    language_info = {
        'name': 'PostgreSQL (pglib)',
        'codemirror_mode': 'sql',
        'mimetype': 'text/x-postgresql',
        'file_extension': '.sql'
    }

    def __init__(self, **kwargs):
        Kernel.__init__(self, **kwargs)
        self.cnxn = None
        logger.info('__init__')

    def do_execute(self, code, silent, store_history=True,
                   user_expressions=None, allow_stdin=False):
        logger.info('execute: %r', code)
        code = code.strip()

        try:
            if code:
                if code.startswith(('connect ', '\c ')):
                    self.connect(code=code)
                elif code == 'help':
                    self.handle_help()
                else:
                    if not self.cnxn:
                        raise APIError('Not connected', 'Use the connect command before executing SQL')
                    self.handle_query(code)

        except APIError as ex:
            # According to the messaging docs, errors should only have to return an error
            # message like below, but nothing is getting displayed.  It could be because we
            # don't have a traceback?
            self.send_response(
                self.iopub_socket, 'stream', {
                    'name': 'stderr',
                    'text': ex.evalue
                })
            return {
                'status': 'error',
                'ename': ex.ename,
                'evalue': ex.evalue
            }

        return {
            'status': 'ok',
            'execution_count': self.execution_count,
            'payload': [],
            'user_expressions': {}
        }

    def send(self, text):
        stream_content = {'name': 'stdout', 'text': text}
        self.send_response(self.iopub_socket, 'stream', stream_content)

    def handle_help(self):
        import sys
        self.send(':'.join(sys.path))

    def handle_query(self, code):
        try:
            result = self.cnxn.execute(code)
        except pglib.Error as ex:
            raise APIError('SQL Error', ex)

        if isinstance(result, pglib.ResultSet):
            header = result.columns
            self.send_response(
                self.iopub_socket, 'display_data',
                {
                    'data': {
                        'text/plain': tabulate(result, header, tablefmt='simple'),
                        'text/html': tabulate(result, header, tablefmt='html'),
                    },
                    'metadata': {}
                })
            return

        if isinstance(result, int):
            if result == 1:
                self.send('1 row affected')
            else:
                self.send('%d rows affected' % result)
            return

    def connect(self, code):
        """
        Returns a pglib connection to the given database.
        """
        self.cnxn = None
        # In case we're replacing a connection and the new one fails.  Later SQL statements
        # should fail, not be sent to the previous connection.

        # Remove the 'connect' keyword.  The rest is passed to pglib.
        cs = code.split(' ', 2)[1]
        logger.info('Connecting to db: %r', cs)

        # If just one word, assume it is a local database name.
        if re.match(r'^[^:/@ ]+$', cs):
            cs = 'dbname=' + cs

        try:
            self.cnxn = pglib.connect(cs)
            self.send('connected')
        except Exception as ex:
            raise APIError('Unable to connect', ex)


class APIError(Exception):
    """
    A exception with the information that needs to be sent back to the client.
    """
    def __init__(self, ename, evalue):
        self.ename = ename
        self.evalue = str(evalue)
        Exception.__init__(self, '%s:%s' % (ename, evalue))
