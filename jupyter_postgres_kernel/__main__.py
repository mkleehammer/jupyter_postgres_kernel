from ipykernel.kernelapp import IPKernelApp
from .kernel import DBKernel
IPKernelApp.launch_instance(kernel_class=DBKernel)
