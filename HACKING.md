
# Testing

## Registering The Kernel

To test, we must first register the kernel with Jupyter so it knows how to start it.  We
register:

    python -m jupyter_postgres_kernel
    
Since jupyter_postgres_kernel is a package, it will look for the jupyter_postgres_kernel.__main__ file and execute
it.

There are two easy ways to register:

- `python setup.py install` will both install in site-packages and register
- If already installed, say with pip, you can use `python -m jupyter_postgres_kernel.install`

Both of these will overwrite the kernel.json file.

## Using the working directory

To test easily, we want to use the code we are working on without having to re-install it.  If
you run `jupyter-notebook` from the root of the directory it will launch `python -m
jupyter_postgres_kernel`.  Python will have added the current directory at the front of `sys.path` and the
current code will be used instead of anything installed in site-packages.

Once it is running in a notebook, you'll need to restart the kernel to pick up code changes.

You can use `jupyter-notebook --debug` to see a lot of useful information, but I haven't yet
figured out how to capture stdout (print statements) or logging.

